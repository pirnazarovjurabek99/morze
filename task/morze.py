def code_morse(value):
  """
  Codes a string to Morse code, ignoring spacebars and separating Morse letters by spacebars.

  Args:
    value: The string to code.

  Returns:
    A string containing the Morse code representation of the input string, with Morse letters separated by spacebars.
  """

  # Create a dictionary of Morse code characters.
  morse_code = {
      'A': '.-',
      'B': '-...',
      'C': '-.-.',
      'D': '-..',
      'E': '.',
      'F': '..-.',
      'G': '--.',
      'H': '....',
      'I': '..',
      'J': '.---',
      'K': '-.-',
      'L': '.-..',
      'M': '--',
      'N': '-.',
      'O': '---',
      'P': '.--.',
      'Q': '--.-',
      'R': '.-.',
      'S': '...',
      'T': '-',
      'U': '..-',
      'V': '...-',
      'W': '.--',
      'X': '-..-',
      'Y': '-.--',
      'Z': '--..',
      '1': '.----',
      '2': '..---',
      '3': '...--',
      '4': '....-',
      '5': '.....',
      '6': '-....',
      '7': '--...',
      '8': '---..',
      '9': '----.',
      '0': '-----',
      ', ': '--..--',
      '.': '.-.-.-',
      '?': '..--..',
      '/': '-..-.',
      '-': '-....-',
      '(': '-.--.',
      ')': '-.--.-',
  }

  # Strip whitespace from the input string.
  value = value.strip()

  # Encode the input string to Morse code.
  morse_code_encoding = ''
  for character in value:
    if character in morse_code:
      morse_code_encoding += morse_code[character] + ' '
  else:
    morse_code_encoding += '?' + ' '

  # Remove the trailing whitespace.
  return morse_code_encoding[:-1]

